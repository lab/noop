FROM alpine:latest as builder
RUN apk add --no-cache build-base
COPY noop.c Makefile ./
RUN make

FROM scratch
COPY --from=builder /noop /
CMD [ "/noop" ]
