# Noop

[![CI](https://github.com/RealOrangeOne/noop/actions/workflows/ci.yml/badge.svg)](https://github.com/RealOrangeOne/noop/actions/workflows/ci.yml)

Do nothing, efficiently.
